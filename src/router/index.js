import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../views/Home.vue'
import PropertyInsurance from '../views/PropertyInsurance.vue'
import MotorInsurance from '../views/MotorInsurance.vue'
import HealthInsurance from '../views/HealthInsurance.vue'
import GeneralInsurance from '../views/GeneralInsurance.vue'
import PageNotFound from '../views/PageNotFound.vue'
import Contacts from '../views/Contacts.vue'

const routes = [
  {
    path: '/',
    component: Home
  },
  {
    path: '/property-insurance',
    component: PropertyInsurance
  },
  {
    path: '/motor-insurance',
    component: MotorInsurance
  },
  {
    path: '/health-insurance',
    component: HealthInsurance
  },
  {
    path: '/general-insurance',
    component: GeneralInsurance
  },
  {
    path: '/contacts',
    component: Contacts
  },
  {
    path: '/:pathMatch(.*)*',
    component: PageNotFound
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
